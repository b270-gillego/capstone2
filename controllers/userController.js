const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Order = require("../models/Order");




module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		if(result.length > 0){
			return res.send("Email already exists!");

		}else {
			return res.send(false);
		}
	})
	.catch(error => res.send(error))
}



module.exports.registerUser = (req, res) => {

	console.log(req.body);
	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: bcrypt.hashSync(req.body.password, 10),

	})

	return newUser.save().then(user => {
		console.log(user);
		res.status(200).json(`Welcome ${req.body.firstName}!`)
	})
	.catch(error => {
		console.log(error);
		res.status(400).json(f`Registration failed`);
	});
}




module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {
		
		if(result == null){

			return res.send({message: "No user found"})

		}else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(result)});

			}else {
				return res.send(false)
			}
		}
	})
}



module.exports.getProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);


	return User.findById(userData.id).then(result => {
		result.password = "";
		return res.send(result);
	})

}


module.exports.checkout = async (req, res) => {
	
  let userData = await User.findByIdAndUpdate(req.body.userId).catch(error =>
    console.log(`Product Data Saving FAILED: ${error}`)
  );

  if (!userData.isAdmin) {
    let productList = [];
    let totalAmount = 0;

    for (let orderProduct of req.body.products) {
      let productData = await Product.findById(orderProduct.productId);
      productData.quantity -= orderProduct.quantity;

      if (
        await productData
          .save()
          .then(product => (console.log(`Product Data Saving SUCCESSFUL`) || true))
          .catch(error => (console.log(`Product Data Saving FAILED: ${error}`) || false))
      ) {
        productList.push({
          id: orderProduct.productId,
          name: productData.name,
          price: productData.price,
          quantity: orderProduct.quantity,
          total: productData.price * orderProduct.quantity,
        });
        totalAmount += productData.price * orderProduct.quantity;
      }
    }

    let newOrder = new Order({
      userId: req.body.userId,
      products: productList,
      totalAmount: totalAmount,
    });

    await newOrder
      .save()
      .then(order => (console.log(`Order Data Saving SUCCESSFUL`) || true))
      .catch(error => (console.log(`Order Data Saving FAILED ${error}`) || false));

    userData.orders.push({ orderId: newOrder._id });

    console.log(userData.orders);

    await userData
      .save()
      .then(user => (console.log(`User Data Saving SUCCESSFUL`) || true))
      .catch(error => (console.log(`User Data Saving FAILED ${error}`) || false));

    return res.send(true);
  } else {
    return res.send(false);
  }
};


// module.exports.checkout = async (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	if(!userData.isAdmin) {
// 		let productName = await Product.findById(req.body.productId).then(result => result.name);

// 		let data = {
// 			userId: userData.id,
// 			email: userData.email,
// 			productId: req.body.productId,
// 		}
// 		console.log(data);

// 		let isUserUpdated = await User.findById(data.userId).then(user => {

// 			user.orders.push({
// 				productId: data.productId,
// 			});

// 			return user.save().then(result => {
// 				console.log(result);
// 				return true;
// 			})
// 			.catch(error => {
// 				console.log(error);
// 				return false;
// 			});
// 		});
// 		console.log(isUserUpdated);

// 		let isProductUpdated = await Product.findById(data.productId)
// 		.then(product => {

// 			product.orders.push({
// 				userId: data.userId
// 			})


// 			return product.save().then(result => {
// 				console.log(result);
// 				return true;
// 			})
// 			.catch(error => {
// 				console.log(error);
// 				return false;
// 			});
// 		});
// 		console.log(isProductUpdated);

// 		if(isUserUpdated && isProductUpdated) {
// 			return res.send(true);

// 		} else {
// 			return res.send(false);
// 		}
// 	} else {
// 		return res.send(false);
// 	}

// }




// module.exports.checkout = async (req, res) => {

//     const userData = auth.decode(req.headers.authorization);

//     if(!userData.isAdmin) {
//         let productName = await Product.findById(req.body.productId).then(result => result.name);

//         let data = {
//             userId: userData.id,
//             email: userData.email,
//             productId: req.body.productId,
// 						 productName: productName
//         }
//         console.log(data);

//         let isUserUpdated = await User.findById(data.userId).then(user => {

//             user.orders.push({
//                 productId: data.productId,
//             });

//             return user.save().then(result => {
//                 console.log(result);
//                 return true;
//             })
//             .catch(error => {
//                 console.log(error);
//                 return false;
//             });
//         });
//         console.log(isUserUpdated);

//         let isProductUpdated = await Product.findById(data.productId)
//         .then(product => {

//             product.orders.push({
//                 userId: data.userId
//             })


//             return product.save().then(result => {
//                 console.log(result);
//                 return true;
//             })
//             .catch(error => {
//                 console.log(error);
//                 return false;
//             });
//         });
//         console.log(isProductUpdated);

//         if(isUserUpdated && isProductUpdated) {
//             return res.send(true);

//         } else {
//             return res.send(false);
//         }
//     } else {
//         return res.send(false);
//     }

// }




// module.exports.createOrder = async (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	if(!userData.isAdmin) {

//  		let productName = await Product.findById(req.body.productId)
//  		.then(result => result.name);

//  		let orderInfo = {

//  			userId: userData.id,
//  			email: userData.email,
//  			productId : req.body.productId,
//  			productName: productName


//  		}
//  		console.log(orderInfo);

//  		let isUserUpdated = await User.findById(orderInfo.userId).then(user => {

//  			user.orders.push({
//  				productId: orderInfo.productId
//  			});

//  			return user.save().then(result => {
//  				console.log(result);
//  				return true;
//  			})
//  			.catch(error => {
//  				console.log(error);
//  				return false
//  			});

//  		});
//  		console.log(isUserUpdated);

//  		let isProductUpdated = await Product.findById(orderInfo.productId)
//  		.then(product => {

//  			product.orders.push({
//  				userId: orderInfo.userId,
 	
//  			})

//  			product.stocks -= 1;

//  			return product.save().then(result => {
//  				console.log(result);
//  				return true;
//  			})
//  			.catch(error => {
//  				console.log(error);
//  				return false;
//  			});

//  		});
//  		console.log(isProductUpdated);

//  		if(isUserUpdated && isProductUpdated) {
//  			return res.send(true);
//  		}else {
//  			return res.send(false);
//  		}
//  	}else {
//  		return res.send(false);
//  	}
//  }