const Product = require("../models/Product");
const auth = require("../auth");


module.exports.createProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		
		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		});


		return newProduct.save()

		.then(product => {
			console.log(product);
			res.send(true);
		})

		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}else{
		return res.send(false);
	}
	
}




module.exports.getAllProducts = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}else{
		return res.send(false);
	}
}




module.exports.getAllActive = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}




module.exports.getProduct = (req, res) => {

	console.log(req.params.productId);
	return Product.findById(req.params.productId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	});
}




module.exports.updateProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.slots
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}else {
		return res.send(false);
	}
}




module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		let archiveProduct = {isActive: req.body.isActive};

		return Product.findByIdAndUpdate(req.params.productId, archiveProduct, {new:true}).then(result => {
			return res.send(true);
		})
		.catch(error => {
			console.log(error);
			return res.send(false);
		})

	}else{
		return res.send(false);
	}

}


