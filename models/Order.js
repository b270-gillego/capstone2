const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema ({

    userId: {
            type: Object,
            default: null
            },
    products:[{
        id:{
            type: Object,
            default: null
            },
        name:{
            type: String,
            required: true
            },
        price:{
            type: Number,
            required: true
            },
        quantity:{
            type: Number,
            required: true
            },
        total:{
            type: Number,
            required: true
            },
        }],
    totalAmount:{
            type: Number,
            default: 0
                },
    purchaseOn:{
            type: Date,
            default:new Date()
                },
});


module.exports = mongoose.model("Order", orderSchema);