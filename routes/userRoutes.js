const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// route for checking if the user's email already exists in the database
router.post("/checkEmail", userController.checkEmailExists);

// route for user registration
router.post("/register", userController.registerUser);

// route for user login
router.post("/login", userController.loginUser);

router.get("/details", userController.getProfile);

router.post("/checkout", auth.verify, userController.checkout);

module.exports = router;